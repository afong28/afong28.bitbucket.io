var files_dup =
[
    [ "controller.py", "controller_8py.html", [
      [ "controller.Controller", "classcontroller_1_1Controller.html", "classcontroller_1_1Controller" ]
    ] ],
    [ "controller_page.py", "controller__page_8py.html", null ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "IMU.py", "IMU_8py.html", "IMU_8py" ],
    [ "lab1_page.py", "lab1__page_8py.html", null ],
    [ "lab2_page.py", "lab2__page_8py.html", null ],
    [ "lab3_page.py", "lab3__page_8py.html", null ],
    [ "lab4_page.py", "lab4__page_8py.html", null ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "report_page.py", "report__page_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_IMU.py", "task__IMU_8py.html", "task__IMU_8py" ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_panel.py", "task__panel_8py.html", "task__panel_8py" ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "touch_panel.py", "touch__panel_8py.html", "touch__panel_8py" ]
];