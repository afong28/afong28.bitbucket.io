var classtouch__panel_1_1Touch__Panel =
[
    [ "__init__", "classtouch__panel_1_1Touch__Panel.html#a3e0831ed83be9e06ba727493c985282b", null ],
    [ "calibrate", "classtouch__panel_1_1Touch__Panel.html#a868de9972436d4eb326ae3509305af9b", null ],
    [ "report", "classtouch__panel_1_1Touch__Panel.html#a5fcdb85aaa27c7e628f2c092656618be", null ],
    [ "update", "classtouch__panel_1_1Touch__Panel.html#a02ff893b91b7e28be20a58950c7b39eb", null ],
    [ "ADC_array", "classtouch__panel_1_1Touch__Panel.html#ab638ca2ede1b5e036e333ecec1d4a685", null ],
    [ "arr_list", "classtouch__panel_1_1Touch__Panel.html#aa9fb6114b599b6f5e1de5bce6c6e8c69", null ],
    [ "cal_array", "classtouch__panel_1_1Touch__Panel.html#ae29853d59e9e5d9526754e61efa45db1", null ],
    [ "cal_values", "classtouch__panel_1_1Touch__Panel.html#ad3bc39781deb93d460f78cca0226826d", null ],
    [ "calib_flag", "classtouch__panel_1_1Touch__Panel.html#a3bf23eb3123d2222beb9549c662982d3", null ],
    [ "coord", "classtouch__panel_1_1Touch__Panel.html#ab6a736f7d5e6f6a0498bbc2704880b93", null ],
    [ "runs", "classtouch__panel_1_1Touch__Panel.html#a8c5e50b2e32d4399054a0b37b2a023df", null ],
    [ "runs_calib", "classtouch__panel_1_1Touch__Panel.html#abf71cbfc212e8286b390fbfba46dee20", null ],
    [ "x", "classtouch__panel_1_1Touch__Panel.html#ac7d707347c18e73eae90e049e6caf452", null ],
    [ "x_ADC", "classtouch__panel_1_1Touch__Panel.html#aba1e48713ff259513f8118f6debfdbdd", null ],
    [ "x_prev", "classtouch__panel_1_1Touch__Panel.html#a4f464b98e921d4bc1e3609df26eac4ff", null ],
    [ "y_ADC", "classtouch__panel_1_1Touch__Panel.html#af7d63e37e425131ca43715769aaddbd3", null ],
    [ "z_ADC", "classtouch__panel_1_1Touch__Panel.html#afcd0deb62791b2e1ea3dc1997573159c", null ]
];